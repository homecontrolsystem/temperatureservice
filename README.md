# Usage

```curl -XGET -H 'Content-Type: application/json' http://localhost:8080/```

# Config

```
/etc/temperatureservice/config.toml

# This is a TOML document.

title = "Temperature service configuration"

[database]
hostname = 'xxx.xx.xxx'
port = 3306
username = 'xxxxx'
password = 'xxxx'
db = 'xxxxx'
```
