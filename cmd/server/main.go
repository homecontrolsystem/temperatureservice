package main

import (
	"fmt"
	"log"
	"net/http"

	"temperatureservice/internal/compute"
	"temperatureservice/internal/service"

	"github.com/gorilla/mux"
	"github.com/spf13/viper"
)

// our main function
func main() {
	viper.SetConfigName("config") // name of config file (without extension)
	viper.SetConfigType("toml")
	viper.AddConfigPath("/etc/temperatureservice/")
	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %s", err))
	}

	dbinfo := viper.GetStringMapString("database")

	port := dbinfo["port"]
	hostname := dbinfo["hostname"]
	username := dbinfo["username"]
	password := dbinfo["password"]
	db := dbinfo["db"]

	connstring := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", username, password, hostname, port, db)
	log.Println(connstring)
	go compute.Start(connstring)
	router := mux.NewRouter()
	router.HandleFunc("/", service.GetTemperature).Methods("GET")
	log.Fatal(http.ListenAndServe(":8080", router))
}
