package compute

import (
	"testing"
	"time"
)

func TestSetTemperature(t *testing.T) {
	var outdoor, indoor float32
	var newIndoor24h, newOutdoor24h [24]float32
	indoor = 0.1
	outdoor = 0.2
	for i := 0; i < 24; i++ {
		newIndoor24h[i] = 0.1 * float32(i)
		newOutdoor24h[i] = 0.2 * float32(i)
	}
	var timestart time.Time
	timestart = time.Now()
	// Test the funtion
	Temperatures.setTemperature(indoor, outdoor, newIndoor24h, newOutdoor24h)
	tdata := Temperatures.Data()

	// Check time that it is between the start and now.
	var timenow time.Time
	timenow = time.Now()
	if timenow.Before(tdata.TimeStamp) {
		t.Errorf("Timestamp incorrect, got: %s.", tdata.TimeStamp)
	}
	if timestart.After(tdata.TimeStamp) {
		t.Errorf("Timestamp incorrect, got: %s.", tdata.TimeStamp)
	}

	if tdata.IndoorTemperature != indoor {
		t.Errorf("Indoor temp was incorrect, got: %f, want: %f.", tdata.IndoorTemperature, indoor)
	}
	if tdata.OutdoorTemperature != outdoor {
		t.Errorf("Outdoor temp was incorrect, got: %f, want: %f.", tdata.OutdoorTemperature, outdoor)
	}
	for i := 0; i < 24; i++ {
		if tdata.IndoorTemperature24h[i] != newIndoor24h[i] {
			t.Errorf("Indoor temp with index %d was incorrect, got: %f, want: %f.", i, tdata.IndoorTemperature24h[i], newIndoor24h[i])
		}
		if tdata.OutdoorTemperature24h[i] != newOutdoor24h[i] {
			t.Errorf("Outdoor temp with index %d was incorrect, got: %f, want: %f.", i, tdata.OutdoorTemperature24h[i], newOutdoor24h[i])
		}
		newIndoor24h[i] = 0.1 * float32(i)
		newOutdoor24h[i] = 0.2 * float32(i)
	}
}
