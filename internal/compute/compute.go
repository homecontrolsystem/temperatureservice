package compute

import (
	"database/sql"
	"fmt"
	"sync"
	"time"

	// Driver for sql
	_ "github.com/go-sql-driver/mysql"
)

// Temperature is exported
type Temperature struct {
	IndoorTemperature     float32
	OutdoorTemperature    float32
	TimeStamp             time.Time
	IndoorTemperature24h  [24]float32
	OutdoorTemperature24h [24]float32
}

// SafeTemperature is safe to use concurrently.
type SafeTemperature struct {
	data Temperature
	mux  sync.Mutex
}

// Temperatures is exported
var Temperatures SafeTemperature

func (t *SafeTemperature) setTemperature(newIndoor float32, newOutdoor float32, newIndoor24h [24]float32, newOutdoor24h [24]float32) {
	t.mux.Lock()
	// Lock so only one goroutine at a time can access data
	t.data.IndoorTemperature = newIndoor
	t.data.OutdoorTemperature = newOutdoor
	t.data.IndoorTemperature24h = newIndoor24h
	t.data.OutdoorTemperature24h = newOutdoor24h
	t.data.TimeStamp = time.Now()
	t.mux.Unlock()
}

// Data returns the current values
func (t *SafeTemperature) Data() Temperature {
	t.mux.Lock()
	// Lock so only one goroutine can access the data
	defer t.mux.Unlock()
	return t.data
}

// Start is the entry point for  service
func Start(connstring string) {
	db, err := sql.Open("mysql", connstring)
	if err != nil {
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	defer db.Close()
	calculateTemperatures(db)
}

func calculateTemperatures(db *sql.DB) (err error) {
	for {
		// Prepare statement for reading data
		rows, err := db.Query("SELECT AVG(temperature) FROM SensorData WHERE (channel = 1 OR channel = 2) AND timestamp > DATE_SUB(NOW(), INTERVAl 5 MINUTE)")
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		rows.Next()
		var outdoor, indoor float32
		rows.Scan(&outdoor)
		rows.Close()
		fmt.Printf("The outdoor: %f", outdoor)
		fmt.Println()
		// Prepare statement for reading data
		rows, err = db.Query("SELECT AVG(temperature) FROM SensorData WHERE (channel = 3 OR channel = 4) AND timestamp > DATE_SUB(NOW(), INTERVAl 5 MINUTE)")
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		rows.Next()
		rows.Scan(&indoor)
		rows.Close()
		fmt.Printf("The indoor: %f", indoor)
		fmt.Println()
		// Read the 24 hour averages
		rows, err = db.Query("SELECT AVG(temperature) as temperature, HOUR(timestamp) as hourtime FROM SensorData WHERE (channel = 1 OR channel = 2) AND DATE_SUB(timestamp, INTERVAL 1 HOUR) AND timestamp > DATE_SUB(NOW(), INTERVAl 1 DAY) GROUP by HOUR(timestamp) ORDER BY timestamp")
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		var newOutdoor24h [24]float32
		var hourtime, i int
		i = 0
		for rows.Next() {
			rows.Scan(&newOutdoor24h[i], &hourtime)
			fmt.Printf("The outdoor 24h: %f - %d", newOutdoor24h[i], hourtime)
			i++
			fmt.Println()
		}
		rows.Close()
		// Read the 24 hour averages
		rows, err = db.Query("SELECT AVG(temperature) as temperature, HOUR(timestamp) as hourtime FROM SensorData WHERE (channel = 3 OR channel = 4) AND DATE_SUB(timestamp, INTERVAL 1 HOUR) AND timestamp > DATE_SUB(NOW(), INTERVAl 1 DAY) GROUP by HOUR(timestamp) ORDER BY timestamp")
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		var newIndoor24h [24]float32
		var hourtime2 int
		i = 0
		fmt.Println()
		for rows.Next() {
			rows.Scan(&newIndoor24h[i], &hourtime2)
			fmt.Printf("The indoor 24h: %f - %d", newIndoor24h[i], hourtime2)
			i++
			fmt.Println()
		}
		rows.Close()
		Temperatures.setTemperature(indoor, outdoor, newIndoor24h, newOutdoor24h)
		fmt.Println()
		time.Sleep(1 * time.Minute)
	}
}
