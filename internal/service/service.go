package service

import (
	"encoding/json"
	"net/http"
	"temperatureservice/internal/compute"
)

// GetTemperature returns the data in JSON
func GetTemperature(w http.ResponseWriter, r *http.Request) {
	//tmp := Temperature{}
	w.Header().Set("Access-Control-Allow-Origin", "*")
	tmp := compute.Temperatures.Data()
	jData, err := json.Marshal(tmp)
	if err != nil {
		// handle error
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(jData)
}
